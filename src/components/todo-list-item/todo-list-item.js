import React from 'react';
import './todo-list-item.css';
const TodoListItem = ({label, important = false}) => {

    const liStyle = {
        color:important? 'steelblue': 'black',
        fontWeight:important? 'bold': 'normal'
    };

    return (
        <span className = "todo-list-item">
            <span className = "todo-list-item-label" style = {liStyle}>{label}</span> 
            <div className = "">
                <button className = "btn btn-outline-success btn-sm  myBtn"><i className = "fa fa-exclamation"></i></button>
                <button className = "btn btn-outline-danger btn-sm "><i className = "fa fa-trash-o"></i></button>
            </div>
        </span>
    );
};

export default TodoListItem;